const crypto = require("crypto");
const got = require("got");
const pkg = require("../package.json");

const secretKey = process.env.DINGTALK_SECRET;
const baseUrl = process.env.DINGTALK_WEBHOOK_URL;

const time = Date.now();
const stringToSign = `${time}\n${secretKey}`;
const signature = crypto
  .createHmac("sha256", secretKey)
  .update(stringToSign)
  .digest()
  .toString("base64");
const url = `${baseUrl}&timestamp=${time}&sign=${signature}`;

const body = `@easyv/react-icons@${pkg.version} has been published, please use 'npm update @easyv/react-icons' to update the package.You can visit http://static-site.easyv.cloud/icons/index.html to see all icons`;

(async () => {
  await got
    .post(url, {
      json: {
        msgtype: "text",
        text: { content: body },
      },
    })
    .json();
})();
