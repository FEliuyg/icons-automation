const got = require("got");
const { ensureDir, writeFile } = require("fs-extra");
const { join, resolve } = require("path");
const Figma = require("figma-js");
const PQueue = require("p-queue");
const upperCamelCase = require("uppercamelcase");

require("dotenv").config();

const { FIGMA_TOKEN, FIGMA_FILE_URL } = process.env;

const options = {
  outputDir: "./src/",
  scale: "1",
};

for (const arg of process.argv.slice(2)) {
  const [param, value] = arg.split("=");
  if (options[param]) {
    options[param] = value;
  }
}

if (!FIGMA_TOKEN) {
  throw Error("Cannot find FIGMA_TOKEN in process!");
}

const client = Figma.Client({
  personalAccessToken: FIGMA_TOKEN,
});

// Fail if there's no figma file key
let fileId = null;
if (!fileId) {
  try {
    fileId = FIGMA_FILE_URL.match(/file\/([a-z0-9]+)\//i)[1];
  } catch (e) {
    throw Error("Cannot find FIGMA_FILE_URL key in process!");
  }
}

console.log(`Exporting ${FIGMA_FILE_URL} components`);

client
  .file(fileId)
  .then(getComponentsFromFigmaData)
  .then(isComponentsNameStandard)
  .then(handleDuplicateComponents)
  .then(insertSvgUrlToComponents)
  .then(writeComponentsData)
  .then(downloadComponents)
  .catch((error) => {
    throw Error(`Error fetching components from Figma: ${error}`);
  });

function queueTasks(tasks, options) {
  const queue = new PQueue(Object.assign({ concurrency: 3 }, options));
  for (const task of tasks) {
    queue.add(task);
  }
  queue.start();
  return queue.onIdle();
}

function getIconType(name) {
  let matches = name.match(/(outlined|filled|colored)$/i);
  if (matches) {
    return matches[1].toLowerCase();
  } else {
    return null;
  }
}

function getComponentsFromFigmaData({ data }) {
  const components = {};

  function check(c, iconType) {
    if (c.type === "COMPONENT") {
      const { name, id } = c;
      const { description = "", key } = data.components[c.id];
      const { width, height } = c.absoluteBoundingBox;

      components[id] = {
        name,
        componentName: upperCamelCase(`${name}-${iconType}`),
        id,
        key,
        file: fileId,
        description,
        width,
        height,
        type: iconType,
      };
    } else if (c.children) {
      // eslint-disable-next-line github/array-foreach
      c.children.forEach((item) => check(item, iconType));
    }
  }

  // filter page that is contain icon and frame is end with outlined 、 filled or colored
  const iconPages = data.document.children.filter(
    (d) => /icon/i.test(d.name) && d.type === "CANVAS"
  );
  const iconFrames = iconPages
    .reduce((all, cur) => all.concat(cur.children), [])
    .filter(
      (d) => /(outlined|filled|colored)$/i.test(d.name) && d.type === "FRAME"
    );
  iconFrames.forEach((item) => check(item, getIconType(item.name)));

  if (Object.values(components).length === 0) {
    throw Error("No components found!");
  }

  console.log(
    `${Object.values(components).length} components found in the figma file`
  );

  return components;
}

function insertSvgUrlToComponents(components) {
  return client
    .fileImages(fileId, {
      format: "svg",
      ids: Object.keys(components),
      scale: options.scale,
    })
    .then(({ data }) => {
      for (const id of Object.keys(data.images)) {
        components[id].image = data.images[id];
      }
      return components;
    });
}

function writeComponentsData(components) {
  return ensureDir(join(options.outputDir))
    .then(() =>
      writeFile(
        resolve(options.outputDir, "../data.json"),
        JSON.stringify(components, null, 2),
        "utf8"
      )
    )
    .then(() => components);
}

function downloadComponents(components) {
  return queueTasks(
    Object.values(components).map((component) => () => {
      return got
        .get(component.image, {
          headers: {
            "Content-Type": "image/svg+xml",
          },
          encoding: "utf8",
        })
        .then((response) => {
          return ensureDir(join(options.outputDir, "svg")).then(() =>
            writeFile(
              join(options.outputDir, "svg", `${component.componentName}.svg`),
              response.body,
              "utf8"
            )
          );
        })
        .catch((err) => {
          console.error(err);
        });
    })
  ).then(() => {
    console.log("all components download successfully");
  });
}

function handleDuplicateComponents(components) {
  let duplicatedComponents = filterDuplicatedComponents(components);
  if (duplicatedComponents.length > 0) {
    throw Error(
      "these components is duplicated:" + JSON.stringify(duplicatedComponents)
    );
  } else {
    return components;
  }
}

function filterDuplicatedComponents(components) {
  const componentsArr = Object.values(components);
  let newArr = [];
  for (let j = 0; j < componentsArr.length; j++) {
    for (let i = j + 1; i < componentsArr.length; i++) {
      if (componentsArr[j].componentName == componentsArr[i].componentName) {
        newArr.push(`${componentsArr[i].name} in ${componentsArr[i].type}`);
      }
    }
  }

  return Array.from(new Set(newArr));
}

function isComponentsNameStandard(components) {
  const nonStandardComponents = Object.values(components)
    .filter((d) => !/^[A-Z][0-9a-zA-Z]+$/.test(d.name))
    .map((d) => d.name);
  if (nonStandardComponents.length > 0) {
    throw Error(
      "these component's name is not standard:" +
        JSON.stringify(nonStandardComponents)
    );
  } else {
    return components;
  }
}
