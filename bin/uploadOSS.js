const OSS = require("ali-oss");
const path = require("path");

require("dotenv").config();

const accessKeyId = process.env.OSS_ACCESS_KEY_ID;
const accessKeySecret = process.env.OSS_ACCESS_KEY_SECRET;

const client = new OSS({
  region: "oss-cn-hangzhou",
  accessKeyId: accessKeyId,
  accessKeySecret: accessKeySecret,
  bucket: "easyv-internal",
});

const files = ["index.html", "index.js"];

files.forEach((item) => {
  client.put(`icons/${item}`, path.resolve(__dirname, `../docs/${item}`));
});
