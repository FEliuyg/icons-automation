/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable prefer-template */
const path = require('path');
const fs = require('fs');
const format = require('prettier-eslint');
const processSvg = require('./processSvg');
const defaultStyle = process.env.npm_package_config_style || 'filled';
const { getAttrs, getElementCode } = require('./template');
const icons = require('../cache.json');

const rootDir = path.join(__dirname, '..');

// where icons code in
const srcDir = path.join(rootDir, 'src');
const svgsDir = path.join(rootDir, 'src/svg');
const iconsDir = path.join(rootDir, 'src/react-icons');

// generate icons.js and icons.d.ts file
const generateIconsIndex = () => {
  if (!fs.existsSync(srcDir)) {
    fs.mkdirSync(srcDir);
    fs.mkdirSync(iconsDir);
  } else if (!fs.existsSync(iconsDir)) {
    fs.mkdirSync(iconsDir);
  }

  const initialTypeDefinitions = `/// <reference types="react" />
  import { ComponentType, SVGAttributes, ReactNode } from 'react';

  interface ConfigProviderProps {
    cls?: string;
    children: ReactNode;
  }

  export const ConfigProvider: ComponentType<ConfigProviderProps>;

  interface Props extends SVGAttributes<SVGElement> {
    color?: string;
    size?: string | number;
  }

  type Icon = ComponentType<Props>;
  `;

  const initialContextCode = `import './index.css';\nexport * from './context.js';\n`;

  fs.writeFileSync(path.join(rootDir, 'src', 'icons.js'), initialContextCode, 'utf-8');
  fs.writeFileSync(path.join(rootDir, 'src', 'icons.d.ts'), initialTypeDefinitions, 'utf-8');
};

// generate attributes code
const attrsToString = (attrs) => {
  return Object.keys(attrs)
    .map((key) => {
      return key + '="' + attrs[key] + '"';
    })
    .join(' ');
};

// generate icon code separately
const generateIconCode = async ({ componentName, type }) => {
  const style = type || defaultStyle;
  const location = path.join(svgsDir, `${componentName}.svg`);
  const destination = path.join(iconsDir, `${componentName}.js`);
  const code = fs.readFileSync(location);
  const svgCode = await processSvg(code, type);

  const element = getElementCode(componentName, attrsToString(getAttrs(style), style), svgCode);
  const component = format({
    text: element,
    eslintConfig: {
      extends: 'airbnb',
    },
    prettierOptions: {
      bracketSpacing: true,
      singleQuote: true,
      parser: 'flow',
    },
  });

  fs.writeFileSync(destination, component, 'utf-8');

  console.log('Successfully built', componentName);
  return { componentName };
};

// append export code to icons.js
const appendToIconsIndex = ({ componentName }) => {
  const exportString = `export { default as ${componentName} } from './react-icons/${componentName}';\r\n`;
  fs.appendFileSync(path.join(rootDir, 'src', 'icons.js'), exportString, 'utf-8');

  const exportTypeString = `export const ${componentName}: Icon;\n`;
  fs.appendFileSync(path.join(rootDir, 'src', 'icons.d.ts'), exportTypeString, 'utf-8');
};

generateIconsIndex();

Object.values(icons).forEach(({ componentName }) => {
  let type = 'outlined';
  if (componentName.endsWith('Filled')) {
    type = 'filled';
  } else if (componentName.endsWith('Colored')) {
    type = 'colored';
  }

  generateIconCode({ componentName, type }).then(({ componentName }) => {
    appendToIconsIndex({ componentName });
  });
});
