const getAttrs = (style) => {
  const filledAttrs = {
    fill: 'currentColor',
  };

  const coloredAttrs = {
    fill: 'none',
  };

  return style === 'filled' || style === 'outlined' ? filledAttrs : coloredAttrs;
};

const getElementCode = (ComponentName, attrs, svgCode) => `
  import React from 'react';
  import PropTypes from 'prop-types';
  import classnames from 'classnames';
  import { useConfig } from '../context';

  const ${ComponentName} = (props) => {
    const { color, size, className, style = {}, ...otherProps } = props;
    const config = useConfig();
    const { cls } = config || {}
    
    return (
      <span className={classnames('easyv-icon', cls, className)} style={{ color, fontSize: size, ...style}} {...otherProps}>
        <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24" ${attrs}>
          ${svgCode}
        </svg>
      </span>
    )
  };

  ${ComponentName}.propTypes = {
    color: PropTypes.string,
    size: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
  }

  export default ${ComponentName}
`;

module.exports = { getAttrs, getElementCode };
