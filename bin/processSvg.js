const Svgo = require("svgo");
const cheerio = require("cheerio");

/**
 * Convert string to CamelCase.
 * @param {string} str - A string.
 * @returns {string}
 */
function CamelCase(str) {
  return str.replace(/(^|-)([a-z])/g, (_, __, c) => c.toUpperCase());
}

/**
 * Optimize SVG with `svgo`.
 * @param {string} svg - An SVG string.
 * @returns {Promise<string>}
 */
function optimize(svg, type) {
  const svgo = new Svgo({
    plugins:
      type === "colored"
        ? []
        : [
            {
              removeAttrs: {
                attrs: "(fill|stroke|fill-opacity|stroke-opacity)",
              },
            },
          ],
  });

  return new Promise((resolve) => {
    svgo.optimize(svg).then(({ data }) => resolve(data));
  });
}

/**
 * remove SVG element.
 * @param {string} svg - An SVG string.
 * @returns {string}
 */
function removeSVGElement(svg) {
  const $ = cheerio.load(svg);
  return $("body")
    .children()
    .html();
}

/**
 * Process SVG string.
 * @param {string} svg - An SVG string.
 * @param {Promise<string>}
 */
async function processSvg(svg, type) {
  const optimized = await optimize(svg, type)
    // remove semicolon inserted by prettier
    // because prettier thinks it's formatting JSX not HTML
    .then((svg) => svg.replace(/;/g, ""))
    .then(removeSVGElement)
    .then((svg) =>
      svg.replace(/-([a-z]+)(?=(-|=))/g, (_, a) => `${CamelCase(a)}`)
    );
  return optimized;
}

module.exports = processSvg;
